﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace AppKiller
{
    class Program
    {
        public static bool ShouldBeLooping = false;
        private static Timer Looper;
        private static List<string> fnames;
        private static bool bDeleteProcess = false;

        static void Main(string[] args)
        {
            Begin:
            Looper = new System.Timers.Timer(250);
            Looper.Elapsed += Looper_Elapsed;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("This will loop forever, and kill any Executables entered.");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Enter each Executable name, comma seperated");
            string input = Console.ReadLine();

            string[] spl = input.Split(',');
            fnames = new List<string>();
            foreach(var name in spl)
            {
                var n = name.Trim();
                if (name.Contains(".exe"))
                {
                   n = name.Replace(".exe", "").Trim();                   
                }                

                if (!fnames.Contains(n))
                    fnames.Add(n);
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Killing the following Processes:");
            Console.ForegroundColor = ConsoleColor.Gray;
            foreach (var nprc in fnames)
            {
                Console.WriteLine(nprc);
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Is This Correct? ");
            string answer = Console.ReadLine();
            if (answer.ToLower().Contains("n") || answer.Contains("0"))
                goto Begin;

            Console.WriteLine("Kill then delete originating Process?");
            var ans  = Console.ReadLine();
            if (ans.ToLower().Contains("y") || ans.Contains("0"))
                bDeleteProcess = true;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Press Any Key To Begin, then Exit or press any key again to restart from beginning.");
            Console.ReadKey();
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("Kill Loop Running");
            Looper.Start();
            ShouldBeLooping = true;
            Console.ReadKey();
            ShouldBeLooping = false;
            Looper.Stop();
            Looper.Dispose();
            goto Begin;
        }

        private static void Looper_Elapsed(object sender, ElapsedEventArgs e)
        {
            if(ShouldBeLooping && fnames.Count > 0)
            {
                foreach(var name in fnames)
                {
                    foreach(var p in Process.GetProcessesByName(name))
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.Write("Killing ");
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.Write(p.ProcessName);
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.Write("...");

                        if (bDeleteProcess)
                        {
                            try
                            {
                                p.Kill();
                                Console.ForegroundColor = ConsoleColor.DarkCyan;
                                Console.Write("...Deleting...");
                                System.IO.File.Delete(p.MainModule.FileName);
                            }
                            catch (Exception ex)
                            {
                                Console.ForegroundColor = ConsoleColor.Cyan;
                                Console.WriteLine(string.Format("Cannot delete {0} | {2}{1}", p.ProcessName, ex.Message, Environment.NewLine));
                            }
                        } else
                        {
                            p.Kill();
                        }
                        Console.WriteLine();
                           

                        
                        
                    }
                }
            }
        }
    }
}
